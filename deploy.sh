#!/bin/bash
# 确保脚本抛出遇到的错误
set -e

# 生成静态文件
npm run docs:build
pwd

# 进入生成的文件夹
currPath=`pwd`
chPath="/docs/.vuepress/dist"

cd "${currPath}${chPath}"
pwd

git init
git add .
git commit -m 'deploy'

# 如果发布到 https://<USERNAME>.github.io/<REPO> https://gitee.com/uglyp/q-ui.git
git push -f https://gitee.com/uglyp/q-ui.git master:gh-pages
# git remote add origin https://gitee.com/uglyp/q-ui.git
# git push origin --delete gh-pages
# git push origin master:gh-pages

cd -
