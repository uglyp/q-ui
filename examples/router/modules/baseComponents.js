/** 基础组件 路由 **/

import Layout from '@/layout'
import Blank from '@/components/Blank'
/* 定义component */
/**
 * 常用组件
 */
// 日期组件
const QDatePicker = () => import('@/views/components/QCommonComponents/QDatePicker')
// 弹窗组件
const QDialog = () => import('@/views/components/QCommonComponents/QDialog')
// input组件
const QInput = () => import('@/views/components/QCommonComponents/QInput')
// 布局组件
const QLayout = () => import('@/views/components/QCommonComponents/QLayout')
// 搜索组件
const QSearch = () => import('@/views/components/QCommonComponents/QSearch')
// 下拉选择
const QSelect = () => import('@/views/components/QCommonComponents/QSelect')
// 步骤组件
const QStepWizard = () => import('@/views/components/QCommonComponents/QStepWizard')
// QTable组件
const QTable = () => import('@/views/components/QTable')
// 条件查询置顶组件
const QQueryCondition = () => import('@/views/components/QQueryCondition')
// 编辑表格
const QEditTable = () => import('@/views/components/QEditTable')

// 图片上传组件
const QUploadFile = () => import('@/views/components/uploadFile')
// TreeTable组件
const TreeTable = () => import('@/views/components/treeTable')
// 表单组件
const QForm = () => import('@/views/components/QForm')
// q-simple-form表单组件
const QSimpleForm = () => import('@/views/components/QForm/q-simple-form')
// 模块表单
const QModuleForm = () => import('@/views/components/QForm/q-module-form')
// 模块详情
const QModuleDetail = () => import('@/views/components/QForm/q-module-detail')

const QUploadExcel = () => import('@/views/components/QUploadExcel')

// const ScrollTable = () => import('@/views/components/scrollTable')

const baseComponentsRouter = {
  path: '/base-components',
  isStatic: true,
  component: Layout,
  name: '基础组件',
  meta: {
    title: '基础组件'
  },
  children: [
    {
      path: 'q-common-components',
      name: '常用组件',
      component: Blank,
      meta: { title: '常用组件' },
      children: [
        {
          path: 'q-date-picker',
          name: '日期组件',
          component: QDatePicker,
          meta: { title: '日期组件' }
        },
        {
          path: 'q-dialog',
          name: '弹窗组件',
          component: QDialog,
          meta: { title: '弹窗组件' }
        },
        {
          path: 'q-input',
          name: 'input组件',
          component: QInput,
          meta: { title: 'input组件' }
        },
        {
          path: 'q-layout',
          name: '布局组件',
          component: QLayout,
          meta: { title: '布局组件' }
        },
        {
          path: 'q-search',
          name: '搜索组件',
          component: QSearch,
          meta: { title: '搜索组件' }
        },
        {
          path: 'q-select',
          name: '下拉选择组件',
          component: QSelect,
          meta: { title: '下拉选择组件' }
        },
        {
          path: 'q-step-wizard',
          name: '步骤组件',
          component: QStepWizard,
          meta: { title: '步骤组件' }
        }
      ]
    },
    // {
    //   path: 'scroll-table',
    //   name: '自动轮询滚动组件',
    //   component: ScrollTable,
    //   meta: { title: '自动轮询滚动组件' }
    // },
    {
      path: 'q-query-condition',
      name: '查询条件置顶组件',
      component: QQueryCondition,
      meta: { title: '查询条件置顶组件' }
    },
    {
      path: 'q-edit-table',
      name: '表格编辑组件',
      component: QEditTable,
      meta: { title: '表格编辑组件' }
    },
    {
      path: 'q-table',
      name: 'QTable组件',
      component: QTable,
      meta: { title: 'QTable组件' }
    },
    {
      path: 'q-upload-file',
      name: '图片上传组件',
      component: Blank,
      meta: { title: '图片上传组件' },
      children: [
        {
          path: 'upload-demo',
          name: '图片上传demo',
          component: QUploadFile,
          meta: { title: '图片上传demo' }
        }
      ]
    },
    {
      path: 'q-tree-table',
      name: 'TreeTable组件',
      component: Blank,
      meta: { title: 'TreeTable组件' },
      children: [
        {
          path: 'tree-table-demo',
          name: 'TreeTableDemo',
          component: TreeTable,
          meta: { title: 'TreeTableDemo' }
        }
      ]
    },
    {
      path: 'q-upload-excel',
      name: '上传excel组件',
      component: Blank,
      meta: { title: '上传excel组件' },
      children: [
        {
          path: 'q-upload-excel-demo',
          name: '上传excel组件',
          component: QUploadExcel,
          meta: { title: '上传excel组件' }
        }
      ]
    },
    {
      path: 'q-form',
      name: '表单组件',
      component: Blank,
      meta: { title: '表单组件' },
      children: [
        {
          path: 'q-form-demo',
          name: '表单组件demo',
          component: QForm,
          meta: { title: '表单组件demo' }
        },
        {
          path: 'q-simple-form-demo',
          name: '精简版表单',
          component: QSimpleForm,
          meta: { title: '精简版表单' }
        },
        {
          path: 'q-module-form-demo',
          name: '模块表单',
          component: QModuleForm,
          meta: { title: '模块表单' }
        },
        {
          path: 'q-module-detail-demo',
          name: '模块详情',
          component: QModuleDetail,
          meta: { title: '模块详情' }
        }
      ]
    }
  ]
}

export default baseComponentsRouter
