const path = require('path')
const port = 2222
const isProduction = process.env.NODE_ENV === 'production'
function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  // transpileDependencies: ['element-ui'], // 解决IE浏览器本地启动白屏现象
  // outputDir: process.env.outputDir || 'dist', // 输出文件名称
  // publicPath: './', // 部署应用包时的基本 URL
  productionSourceMap: !isProduction, // 解决vue项目打包后浏览器F12查看到项目源代码false不能看到
  // productionSourceMap: true, // 测试调试打断点
  // lintOnSave: false,// 去掉eslint校验
  devServer: {
    port: port, // 设置端口号
    // open: true, // 启动项目自动打开浏览器
    proxy: {
      // detail: https://cli.vuejs.org/config/#devserver-proxy
      [process.env.VUE_APP_BASE_API]: {
        target: `http://192.168.0.106:10001/redPacket/`, // 开发环境
        // target: `http://192.168.20.203:8080/`,  // 测试环境地址http://carbontest.lcago.cn:10088
        // target: `http://carbontest.lcago.cn:10088/`,  // 测试环境地址http://carbontest.lcago.cn:10088
        // target: `http://172.20.13.172:8080`,  // 生产环境-old
        // target: `https://yqb.dcps.top/mansmartpay/`,  // 生产环境
        // target: `http://192.168.20.187:8080/`,     //后台人员张春月本地环境
        // target: `http://192.168.20.119:8080/`,     //后台人员李腾本地环境

        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: ''
        }
      }
    }
  },
  // 修改 src 目录 为 examples 目录
  pages: {
    index: {
      entry: 'examples/main.js',
      template: 'public/index.html',
      filename: 'index.html'
    }
  },
  // 强制内联CSS
  css: {
    extract: false
  },
  configureWebpack: (config) => {
    config.resolve.alias['@'] = resolve('examples')
    config.resolve.alias['components'] = resolve('examples/components')
    config.resolve.alias['~'] = resolve('packages')
    // 生产环境配置
    if (isProduction) {
      config.mode = 'production'
      // 打包文件大小配置
      config.performance = {
        maxEntrypointSize: 10000000,
        maxAssetSize: 30000000
      }
    }
  },
  // 底层是webpack-chain packages
  chainWebpack: (config) => {
    // 配置兼容IE浏览器
    // config.entry.app = ['babel-polyfill', './src/main.js']
    // 配置别名
    // vue默认@指向src目录，这里要修正为examples，另外新增一个~指向packages
    // config.resolve.alias
    //   .set('@', path.resolve('examples'))
    //   .set('~', path.resolve('packages'))
    // lib目录是组件库最终打包好存放的地方，不需要eslint检查
    // examples/docs是存放md文档的地方，也不需要eslint检查
    // config.module
    //   .rule('eslint')
    //   .exclude.add(path.resolve('lib'))
    //   .end()
    // packages和examples目录需要加入编译
    // config.module
    //   .rule('eslint')
    //   .exclude.add(path.resolve('lib'))
    //   .end()
    //   .rule('js')
    //   .include.add('/packages/')
    //   .end()
    //   //   .include.add('/examples/')
    //   //   .end()
    //   .use('babel')
    //   .loader('babel-loader')
    //   .tap(options => {
    //     // 修改它的选项...
    //     return options
    //   })
    // 生产环境配置
    // if (isProduction) {
    //   // 删除预加载
    //   config.plugins.delete('preload')
    //   config.plugins.delete('prefetch')
    //   // 压缩代码
    //   config.optimization.minimize(true)
    //   // 分割代码
    //   config.optimization.splitChunks({
    //     chunks: 'all'
    //     // maxSize: 100000 // 大于100kb做二次分割
    //   })
    // }
  }
}
