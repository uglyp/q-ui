# Qui

## 简介

基于[Element-ui](https://element.faas.ele.me/#/zh-CN)的B端基础组件库。


## npm 方式安装使用

```js

npm i @nostalgia1/q-ui

import Qui from '@nostalgia1/q-ui'

Vue.use(Qui)


```

## 安装依赖
```shell
npm install

yarn install

```

## example

```shell
npm run serve
yarn serve
```

## docs

```shell
npm run docs:dev
yarn docs:dev

```
