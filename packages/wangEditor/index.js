import QEditor from './src'

/* istanbul ignore next */
QEditor.install = function (Vue) {
  Vue.component(QEditor.name, QEditor)
}

export default QEditor
