import QUploadFile from './src'

/* istanbul ignore next */
QUploadFile.install = function (Vue) {
  Vue.component(QUploadFile.name, QUploadFile)
}

export default QUploadFile
