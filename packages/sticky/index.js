import QSticky from './src'

/* istanbul ignore next */
QSticky.install = function (Vue) {
  Vue.component(QSticky.name, QSticky)
}

export default QSticky
