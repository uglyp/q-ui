import QPaginationSelect from './src'

/* istanbul ignore next */
QPaginationSelect.install = function (Vue) {
  Vue.component(QPaginationSelect.name, QPaginationSelect)
}

export default QPaginationSelect
