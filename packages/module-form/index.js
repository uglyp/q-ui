import QModuleForm from './src'

/* istanbul ignore next */
QModuleForm.install = function (Vue) {
  Vue.component(QModuleForm.name, QModuleForm)
}

export default QModuleForm
