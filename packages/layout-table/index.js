import QAntLayoutTable from './src'

/* istanbul ignore next */
QAntLayoutTable.install = function (Vue) {
  Vue.component(QAntLayoutTable.name, QAntLayoutTable)
}

export default QAntLayoutTable
