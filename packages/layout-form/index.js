import QAntLayoutForm from './src'

/* istanbul ignore next */
QAntLayoutForm.install = function (Vue) {
  Vue.component(QAntLayoutForm.name, QAntLayoutForm)
}

export default QAntLayoutForm
