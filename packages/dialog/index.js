import QDialog from './src'

/* istanbul ignore next */
QDialog.install = function (Vue) {
  Vue.component(QDialog.name, QDialog)
}

export default QDialog
