import QTable from './src'

/* istanbul ignore next */
QTable.install = function (Vue) {
  Vue.component(QTable.name, QTable)
}

export default QTable
