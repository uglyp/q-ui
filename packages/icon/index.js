import QIcon from './src'

/* istanbul ignore next */
QIcon.install = function (Vue) {
  Vue.component(QIcon.name, QIcon)
}

export default QIcon
