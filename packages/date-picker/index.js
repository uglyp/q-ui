import QDatePicker from './src'

/* istanbul ignore next */
QDatePicker.install = function (Vue) {
  Vue.component(QDatePicker.name, QDatePicker)
}

export default QDatePicker
