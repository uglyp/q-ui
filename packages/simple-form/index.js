import QSimpleForm from './src'

/* istanbul ignore next */
QSimpleForm.install = function (Vue) {
  Vue.component(QSimpleForm.name, QSimpleForm)
}

export default QSimpleForm
