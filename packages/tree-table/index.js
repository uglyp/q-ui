import QTreeTable from './src'

/* istanbul ignore next */
QTreeTable.install = function (Vue) {
  Vue.component(QTreeTable.name, QTreeTable)
}

export default QTreeTable
