import QForm from './src'

/* istanbul ignore next */
QForm.install = function (Vue) {
  Vue.component(QForm.name, QForm)
}

export default QForm
