import QComplexEditTable from './src'

/* istanbul ignore next */
QComplexEditTable.install = function (Vue) {
  Vue.component(QComplexEditTable.name, QComplexEditTable)
}

export default QComplexEditTable
