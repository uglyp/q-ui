import QProtocol from './src'

/* istanbul ignore next */
QProtocol.install = function (Vue) {
  Vue.component(QProtocol.name, QProtocol)
}

export default QProtocol
