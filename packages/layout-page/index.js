import QLayoutPage from './src'

/* istanbul ignore next */
QLayoutPage.install = function (Vue) {
  Vue.component(QLayoutPage.name, QLayoutPage)
}

export default QLayoutPage
