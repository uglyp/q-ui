import QSearch from './src'

/* istanbul ignore next */
QSearch.install = function (Vue) {
  Vue.component(QSearch.name, QSearch)
}

export default QSearch
