import QSelect from './src'

/* istanbul ignore next */
QSelect.install = function (Vue) {
  Vue.component(QSelect.name, QSelect)
}

export default QSelect
