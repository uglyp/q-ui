import QPhone from './src'

/* istanbul ignore next */
QPhone.install = function (Vue) {
  Vue.component(QPhone.name, QPhone)
}

export default QPhone
