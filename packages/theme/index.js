import QTheme from './src'

/* istanbul ignore next */
QTheme.install = function (Vue) {
  Vue.component(QTheme.name, QTheme)
}

export default QTheme
