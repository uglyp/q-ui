import QRemark from './src'

/* istanbul ignore next */
QRemark.install = function (Vue) {
  Vue.component(QRemark.name, QRemark)
}

export default QRemark
