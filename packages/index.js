import QAntConfigform from './config-form'
import QAntLayoutConditional from './layout-conditional'
import QLayoutPage from './layout-page'
import QLayoutPageItem from './layout-page-item'
import QAntLayoutTable from './layout-table'
import QAntRangePicker from './range-picker'
import QCalendar from './calendar'
import QDatePicker from './date-picker'
import QDialog from './dialog'
import QEditTable from './edit-table'
import QComplexEditTable from './complex-edit-table'
import QForm from './form'
import QIcon from './icon'
import QInput from './input'
import QInputSearch from './input-search'
import QLayout from './layout'
import QPhone from './phone'
import QProtocol from './protocol'
import QQueryCondition from './query-condition'
import QSearch from './search'
// import QSimpleForm from './simple-form'
import QStepWizard from './step-wizard'
import QSticky from './sticky'
import QTable from './table'
import QTimerBtn from './timer-btn'
import QTreeTable from './tree-table'
// import QUploadExcel from './upload-excel'
import QUploadFile from './upload-file'
import UploadFile from './UploadFile'
import QAntLayoutForm from './layout-form'
import QModuleForm from './module-form'
import QBtnDate from './btn-date'
import QSelect from './select'
import QPaginationSelect from './pagination-select'
import QDetail from './detail'
import { QAntModal, QAntProtocol } from './modal'
import QSelectTable from './select-table'
import QTreeSelect from './tree-select'
import QButton from './button'
import QEditor from './wangEditor'
// import QTheme from './theme'
import { version } from '../package.json'
// 存储组件列表
const components = [
  QAntConfigform,
  QAntLayoutConditional,
  QLayoutPage,
  QLayoutPageItem,
  QAntLayoutTable,
  QAntRangePicker,
  QCalendar,
  QDialog,
  QDatePicker,
  QForm,
  QIcon,
  QInput,
  QInputSearch,
  QLayout,
  QPhone,
  QProtocol,
  QQueryCondition,
  QSearch,
  // QSimpleForm,
  QStepWizard,
  QSticky,
  QTable,
  QTimerBtn,
  QTreeTable,
  // QUploadExcel,
  QUploadFile,
  UploadFile,
  QAntLayoutForm,
  QAntModal,
  QAntProtocol,
  QModuleForm,
  QComplexEditTable,
  QEditTable,
  QBtnDate,
  QSelect,
  QPaginationSelect,
  QDetail,
  QSelectTable,
  QButton,
  QTreeSelect,
  QEditor
  // QTheme
]

// 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，则所有的组件都将被注册
const install = function (Vue) {
  // 判断是否安装
  if (install.installed) return
  install.installed = true
  // 遍历注册全局组件
  components.map(component => Vue.component(component.name, component))
}

// 判断是否是直接引入文件
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}
export default {
  version,
  ...components, // 按需引入
  // 导出的对象必须具有 install，才能被 Vue.use() 方法安装
  install
}
