import QCalendar from './src'

/* istanbul ignore next */
QCalendar.install = function (Vue) {
  Vue.component(QCalendar.name, QCalendar)
}

export default QCalendar
