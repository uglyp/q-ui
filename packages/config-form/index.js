import QAntConfigform from './src'

/* istanbul ignore next */
QAntConfigform.install = function (Vue) {
  Vue.component(QAntConfigform.name, QAntConfigform)
}

export default QAntConfigform
