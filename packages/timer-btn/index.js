import QTimerBtn from './src'

/* istanbul ignore next */
QTimerBtn.install = function (Vue) {
  Vue.component(QTimerBtn.name, QTimerBtn)
}

export default QTimerBtn
