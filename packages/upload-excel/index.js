import QUploadExcel from './src'

/* istanbul ignore next */
QUploadExcel.install = function (Vue) {
  Vue.component(QUploadExcel.name, QUploadExcel)
}

export default QUploadExcel
