import QAntModal from './src'
import QAntProtocol from './src/protocol.vue'

/* istanbul ignore next */
QAntModal.install = function (Vue) {
  Vue.component(QAntModal.name, QAntModal)
}

export {
  QAntModal,
  QAntProtocol
}
