import QLayoutPageItem from './src'

/* istanbul ignore next */
QLayoutPageItem.install = function (Vue) {
  Vue.component(QLayoutPageItem.name, QLayoutPageItem)
}

export default QLayoutPageItem
