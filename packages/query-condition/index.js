import QQueryCondition from './src'

/* istanbul ignore next */
QQueryCondition.install = function (Vue) {
  Vue.component(QQueryCondition.name, QQueryCondition)
}

export default QQueryCondition
