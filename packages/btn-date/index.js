import QBtnDate from './src'

/* istanbul ignore next */
QBtnDate.install = function (Vue) {
  Vue.component(QBtnDate.name, QBtnDate)
}

export default QBtnDate
