import QLayout from './src'

/* istanbul ignore next */
QLayout.install = function (Vue) {
  Vue.component(QLayout.name, QLayout)
}

export default QLayout
