# 布局组件
---

<common-code-format description="在组件中配置：添加sectionTitle显示头部标题(若不添加该属性则默认显示面包屑)，添加属性returnBtn，显示返回按钮（可以自己定义@click点击事件）；此布局组件可根据屏幕高度自适应适配">
  <docsComponents-QLayout-index></docsComponents-QLayout-index>
  <highlight-code slot="codeText">
    <template>
        <div class="q-layout-demo" style="width:100%;">
          <q-layout returnBtn></q-layout>
        </div>
    </template>
  </highlight-code>
</common-code-format>
