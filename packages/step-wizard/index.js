import QStepWizard from './src'

/* istanbul ignore next */
QStepWizard.install = function (Vue) {
  Vue.component(QStepWizard.name, QStepWizard)
}

export default QStepWizard
