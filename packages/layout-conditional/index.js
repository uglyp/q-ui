import QAntLayoutConditional from './src'

/* istanbul ignore next */
QAntLayoutConditional.install = function (Vue) {
  Vue.component(QAntLayoutConditional.name, QAntLayoutConditional)
}

export default QAntLayoutConditional
