import QSelectTable from './src'

/* istanbul ignore next */
QSelectTable.install = function (Vue) {
  Vue.component(QSelectTable.name, QSelectTable)
}

export default QSelectTable
