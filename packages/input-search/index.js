import QInputSearch from './src'

/* istanbul ignore next */
QInputSearch.install = function (Vue) {
  Vue.component(QInputSearch.name, QInputSearch)
}

export default QInputSearch
