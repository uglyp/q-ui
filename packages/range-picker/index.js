import QAntRangePicker from './src'

/* istanbul ignore next */
QAntRangePicker.install = function (Vue) {
  Vue.component(QAntRangePicker.name, QAntRangePicker)
}

export default QAntRangePicker
