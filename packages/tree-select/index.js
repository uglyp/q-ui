import QTreeSelect from './src'

/* istanbul ignore next */
QTreeSelect.install = function (Vue) {
  Vue.component(QTreeSelect.name, QTreeSelect)
}

export default QTreeSelect
