import QInput from './src'

/* istanbul ignore next */
QInput.install = function (Vue) {
  Vue.component(QInput.name, QInput)
}

export default QInput
