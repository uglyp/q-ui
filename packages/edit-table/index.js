import QEditTable from './src'

/* istanbul ignore next */
QEditTable.install = function (Vue) {
  Vue.component(QEditTable.name, QEditTable)
}

export default QEditTable
