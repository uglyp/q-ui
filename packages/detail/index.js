import QDetail from './src'

/* istanbul ignore next */
QDetail.install = function (Vue) {
  Vue.component(QDetail.name, QDetail)
}

export default QDetail
