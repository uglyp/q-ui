---
home: true
heroImage: /hero.jpg
actionText: get started
actionLink: /baseComponents/
features:
  - title: 架构思想
    details: 遵循统一原则，把简单的东西想复杂、把复杂的东西做简单、把复杂的东西讲简单。
  - title: 前端规范
    details: 规范先行，遵循约定大于配置原则。
  - title: 问题总结复盘
    details: 记录前端常见问题，便于总结复盘，好记性不如烂笔头～。
  - title: 在线体验
    details: 直观展示组件使用方式，对新人友好。文档集成组件 Demo和组合用例，并附代码，快速体验交互细节。
  - title: 渐进式原则
    details: 前端技术迭代遵循渐进式原则，根据人员技术水平，在不影响业务支撑的前提下，逐步迭代新的技术方案。
  - title:
    details:
footer: ©2020-2023
---
