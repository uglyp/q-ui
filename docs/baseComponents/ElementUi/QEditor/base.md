# QEditor 组件

---

## 单独使用

<common-code-format>
  <docsComponents-QEditor-index slot="source"></docsComponents-QEditor-index>
  <<< @/docs/.vuepress/components/docsComponents/QEditor/index.vue
</common-code-format>

## 配合 q-form 使用

<common-code-format>
  <docsComponents-QEditor-inForm slot="source"></docsComponents-QEditor-inForm>
  <<< @/docs/.vuepress/components/docsComponents/QEditor/inForm.vue
</common-code-format>

## 配置参数（Attributes

| 参数        | 说明               | 类型    | 默认值 |
| :---------- | :----------------- | :------ | -----: |
| v-model     | 绑定值             | string  |     无 |
| height      | 编辑器的高         | Number  |    400 |
| placeholder | 默认提示语         | string  |     无 |
| autoFocus   | 是否自动聚焦       | Boolean |  false |
| videoServer | 上传视频的服务接口 | String  |     无 |
| imageServer | 上传图片的服务接口 | String  |     无 |
| meta        | 请求自定义参数     | Object  |     {} |
| headers     | 自定义请求头       | Object  |     {} |
