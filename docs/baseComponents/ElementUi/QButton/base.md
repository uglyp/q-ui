# button按钮组件(内置防抖功能)

---

<common-code-format>
  <docsComponents-QButton-index slot="source"></docsComponents-QButton-index>
在组件中需配置：

`继承el-button所有属性` <br/>

新增`time`属性（多少时间内点击；默认 1 秒）<br/>

<<< @/docs/.vuepress/components/docsComponents/QButton/index.vue
</common-code-format>
