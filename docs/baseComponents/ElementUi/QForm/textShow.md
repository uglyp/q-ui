# 文本展示

---

<common-code-format>
  <docsComponents-QForm-textShow slot="source"></docsComponents-QForm-textShow>
  `QForm`组件提供了`文本展示`的功能

设置 `textShow: true`; `textValue`——显示的文本

<<< @/docs/.vuepress/components/docsComponents/QForm/textShow.vue
</common-code-format>
