# 赋值回显数据

---

<common-code-format>
  <docsComponents-QForm-assignment slot="source"></docsComponents-QForm-assignment>

获取后台接口直接赋值：`this.formOpts.formData`

<<< @/docs/.vuepress/components/docsComponents/QForm/assignment.vue
</common-code-format>
