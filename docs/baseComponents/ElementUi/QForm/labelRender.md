# 自定义 label jsx 渲染

---

<common-code-format>
  <docsComponents-QForm-labelRender slot="source"></docsComponents-QForm-labelRender>

`QForm`组件提供了自定义 `label`的功能，使用`jsx`方式—————配置`labelRender`

**当`labelRender`与`label`同时存在时：优先渲染`labelRender`**

<<< @/docs/.vuepress/components/docsComponents/QForm/labelRender.vue
</common-code-format>
