# 对象模式渲染数据 功能(适用于一层对象数据)

---

<common-code-format>
  <docsComponents-QTable-objRenderPorp slot="source"></docsComponents-QTable-objRenderPorp>
  需配置

`isObjShowProp`: 是否开启对象模式渲染数据

<<< @/docs/.vuepress/components/docsComponents/QTable/objRenderPorp.vue
</common-code-format>
