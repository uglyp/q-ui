# 基本使用

---

<common-code-format>
  <docsComponents-QTable-base slot="source"></docsComponents-QTable-base>
  <<< @/docs/.vuepress/components/docsComponents/QTable/base.vue
</common-code-format>

# 双击单元格复制

---

<common-code-format>
  <docsComponents-QTable-copy slot="source"></docsComponents-QTable-copy>
  在组件中需配置
  
  新增`isCopy`或`isCopy='true'`属性

<<< @/docs/.vuepress/components/docsComponents/QTable/copy.vue
</common-code-format>

# 文字变色

---

<common-code-format>
  <docsComponents-QTable-textColor slot="source"></docsComponents-QTable-textColor>
  在组件中需配置
  
  `changeColor`对象

<<< @/docs/.vuepress/components/docsComponents/QTable/textColor.vue
</common-code-format>

# 第一列显示序列号

---

<common-code-format description="在组件中需配置：firstColumn: { type: 'index', label: '序列' }">
  <docsComponents-QTable-sequence slot="source"></docsComponents-QTable-sequence>
在组件中需配置

`firstColumn`: { type: 'index', label: '序列' }

<<< @/docs/.vuepress/components/docsComponents/QTable/sequence.vue

</common-code-format>

# 第一列显示单选项

---

<common-code-format description="在组件中需配置：firstColumn: { type: 'index', label: '序列' }">
  <docsComponents-QTable-radio slot="source"></docsComponents-QTable-radio>
在组件中需配置

`firstColumn`: { type: 'radio' }<br/>
`@radioChange="radioChange"`事件传出选中是数据

<<< @/docs/.vuepress/components/docsComponents/QTable/radio.vue

</common-code-format>
