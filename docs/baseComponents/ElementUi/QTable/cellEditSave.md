# 单元格编辑保存单行操作

---

<common-code-format>
  <docsComponents-QTable-cellEditSave slot="source"></docsComponents-QTable-cellEditSave>
   
   主要在`configEdit`中配置bind(`函数`返回整条数据)
   
<<< @/docs/.vuepress/components/docsComponents/QTable/cellEditSave.vue
</common-code-format>
