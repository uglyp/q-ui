# QTable 集成 Tree 结构

---

<common-code-format>
  <docsComponents-QTable-tree slot="source"></docsComponents-QTable-tree>
在组件中需配置：

`isShowTreeStyle`是否开启 tree 树形结构样式

必须要指定 `row-key`

必须配置`tree-props`

其它属性可以参考 el-table 属性；如默认展开设置：`default-expand-all`

<<< @/docs/.vuepress/components/docsComponents/QTable/tree.vue
</common-code-format>
