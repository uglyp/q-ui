# 分页功能

---

<common-code-format>
  <docsComponents-QTable-pagination slot="source"></docsComponents-QTable-pagination>
  在组件中需配置
  
  新增`isShowPagination`或`isShowPagination='true'`属性
  
  <<< @/docs/.vuepress/components/docsComponents/QTable/pagination.vue
</common-code-format>
