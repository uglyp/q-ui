# 显示隐藏列

---

<common-code-format>
  <docsComponents-QTable-columnSet slot="source"></docsComponents-QTable-columnSet>
  在组件中需配置：
属性`columnSetting`是否显示列设置按钮

<<<@/docs/.vuepress/components/docsComponents/QTable/columnSet.vue
</common-code-format>
