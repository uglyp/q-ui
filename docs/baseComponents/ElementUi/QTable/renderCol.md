# 某列 render 渲染功能

---

<common-code-format>
  <docsComponents-QTable-renderCol slot="source"></docsComponents-QTable-renderCol>
  需配置

通常用来根据字典转换中文

`columns`: `columns`某一项添加 render 函数（传值：当前值、整行数据、第几行）

<<< @/docs/.vuepress/components/docsComponents/QTable/renderCol.vue
</common-code-format>
