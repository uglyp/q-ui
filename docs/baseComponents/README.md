# 安装&使用


q-ui 是基于 vue2 + Element-ui B端基础组件库，能满足日常的B端业务开发，提升开发效率。


## Install

```js
npm install @nostalgia1/q-ui -S
&
yarn add @nostalgia1/q-ui -S
```

## Usage

```js

import Qui from "@nostalgia1/q-ui"

Vue.use(Qui)
```
