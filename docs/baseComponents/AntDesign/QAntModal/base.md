# ant 弹窗组件

---

## 基础弹窗

<common-code-format>
  <docsComponents-QAntModal-index slot="source"></docsComponents-QAntModal-index>
  <<< @/docs/.vuepress/components/docsComponents/QAntModal/index.vue
</common-code-format>

## 协议弹窗

<common-code-format>
  <docsComponents-QAntModal-protocol slot="source"></docsComponents-QAntModal-protocol>
  <<< @/docs/.vuepress/components/docsComponents/QAntModal/protocol.vue
</common-code-format>
