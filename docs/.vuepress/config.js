module.exports = {
  title: "前端文档",
  description: "前端规范文档、组件交互演示文档、问题汇总。",
  base: "/",
  head: [
    [
      "link",
      {
        rel: "icon",
        href: "/assets/img/logo.png",
      },
    ],
    [
      "meta",
      {
        name: "viewport",
        content: "width=device-width,initial-scale=1,user-scalable=no",
      },
    ],
    ["script", { src: "/assets/js/bodyClick.js" }],
  ],
  markdown: {
    // lineNumbers: true // 代码块显示行号
  },
  themeConfig: {
    lastUpdated: "上次更新",
    // 你的GitHub仓库，请正确填写
    // repo: "https://github.com/uglyp/q-ui",
    // 自定义仓库链接文字。
    //repoLabel: "GitHub",
    nav: [
      { text: "主页", link: "/" },
      { text: "基础组件", link: "/baseComponents/" },
      { text: "前端规范", link: "/frontEndSpecification/" },
      { text: "项目总结", link: "/projectProblem/" },
      // { text: '更新日志', link: '/changeLog/log' },
      // { text: "Gitee码云", link: "https://gitee.com/uglyp/q-ui" },
    ],
    sidebar: {
      "/frontEndSpecification/": [
        {
          title: "前端规范",
          collapsable: false,
          children: ["git", "vueGuide"],
        },
      ],
      "/baseComponents/": [
        {
          title: "基于element封装",
          collapsable: true,
          children: [
            {
              title: "常用组件",
              collapsable: false,
              children: [
                // 'ElementUi/QTheme/base', // 主题设置
                "ElementUi/QInput/base", // input组件
                "ElementUi/QButton/base", // button组件
                "ElementUi/QSearch/base", // 下拉搜索查询组件
                "ElementUi/QLayout/base", // 布局组件
                "ElementUi/QLayoutPage/base", // 局部布局组件
                "ElementUi/QStepWizard/base", // 步驟组件
                "ElementUi/QDatePicker/base", // 日期组件
                "ElementUi/QDialog/base", // 弹窗组件
                "ElementUi/QDetail/base", // 详情组件
                "ElementUi/QTreeSelect/base", // 下拉选择树形及结构组件
                "ElementUi/QSelect/base", // 下拉选择组件
                "ElementUi/QPaginationSelect/base", // 下拉选择分页组件
                "ElementUi/QSelectTable/base", // 下拉选择表格组件
                "ElementUi/QEditor/base", //
                // 'ElementUi/QUploadExcel/base' // 批量上传excel组件
              ],
            },
            {
              title: "查询条件组件",
              collapsable: false,
              children: [
                "ElementUi/QQueryCondition/base", // 基本
                "ElementUi/QQueryCondition/slot", // 插槽使用
                "ElementUi/QQueryCondition/labelRender", // tsx自定义label使用
                "ElementUi/QQueryCondition/defaultVal", // 默认值
                "ElementUi/QQueryCondition/linkage", // 联动使用
                "ElementUi/QQueryCondition/QSelectUse", // 结合QSelect使用
                "ElementUi/QQueryCondition/QDatePickerUse", // 结合QDatePicker使用
                "ElementUi/QQueryCondition/help", // 使用帮助(Attributes)
              ],
            },
            {
              title: "表单组件",
              collapsable: false,
              children: [
                "ElementUi/QForm/base", // 基本模式
                "ElementUi/QForm/labelPosition", // 对齐方式
                "ElementUi/QForm/widthSize", // 每行展示多少项
                "ElementUi/QForm/rules", // 表单验证
                "ElementUi/QForm/labelRender", // 自定义label
                "ElementUi/QForm/slotName", // 自定义输入框插槽
                "ElementUi/QForm/textShow", // 文本展示
                "ElementUi/QForm/comUse", // 自己组件使用
                "ElementUi/QForm/assignment", // 赋值回显
                "ElementUi/QForm/submit", // 切换页面获取表单数据
                "ElementUi/QForm/help", // 使用帮助(Attributes)
                "ElementUi/QModuleForm/base", // 基本
                "ElementUi/QModuleForm/detail", // detail
                "ElementUi/QModuleForm/help", // 使用帮助(Attributes)
              ],
            },
            {
              title: "QTable组件",
              collapsable: false,
              children: [
                // 'ElementUi/QTable/scroll', // 轮询滚动
                "ElementUi/QTable/base", // 基本
                "ElementUi/QTable/pagination", // 分页
                "ElementUi/QTable/notSort", // 指定列不需要排序
                "ElementUi/QTable/mergedCell", // 合并单元格
                "ElementUi/QTable/headerGroup", // 表头合并
                "ElementUi/QTable/renderCol", // render列渲染
                "ElementUi/QTable/filters", // 字典过滤
                "ElementUi/QTable/objRenderPorp", // 对象模式渲染
                "ElementUi/QTable/renderHeader", // 自定义列标题 label
                "ElementUi/QTable/radio", // 单选
                "ElementUi/QTable/singleEditKeyup", // 单元格编辑键盘事件
                "ElementUi/QTable/singleEditTable", // 开启单个单元格编辑
                "ElementUi/QTable/cellEditSave", // 单元格编辑保存单行操作
                "ElementUi/QTable/editTable", // 开启整行编辑模式
                "ElementUi/QTable/checkbox", // 基本复选
                "ElementUi/QTable/columnSet", // 显示隐藏列
                "ElementUi/QTable/operation", // 操作栏
                "ElementUi/QTable/customRender", // customRender操作自定义渲染
                "ElementUi/QTable/tree", // TreeTable组件
                "ElementUi/QTable/help", // 使用帮助(Attributes)
              ],
            },
            // {
            //   title: '表格编辑组件',
            //   collapsable: false,
            //   children: [
            //     'ElementUi/QEditTable/base', // 编辑table组件
            //   ]
            // },
            // {
            //   title: 'QTreeTable组件',
            //   collapsable: false,
            //   children: [
            //     'ElementUi/QTable/tree' // TreeTable组件
            //   ]
            // },
            {
              title: "图片/文件上传组件",
              collapsable: false,
              children: [
                "ElementUi/QUploadFile/base",
                "ElementUi/UploadFile/base",
              ],
            },
          ],
        },
        // {
        //   title: "基于AntDesign封装",
        //   collapsable: true,
        //   children: [
        //     {
        //       title: "常用组件",
        //       collapsable: false,
        //       children: [
        //         "AntDesign/QAntLayoutConditional/base", // 筛选器布局组件
        //         "AntDesign/QAntLayoutTable/base", // 默认table布局组件
        //         "AntDesign/QAntLayoutForm/base", // 配置化表单组件
        //         "AntDesign/QAntModal/base", // 弹窗组件
        //       ],
        //     },
        //   ],
        // },
      ],
      "/projectProblem/": [
        {
          title: "Vue项目",
          collapsable: false,
          // children: ["permission", "keepAlive", "axios"],
        },
      ],
      // '/theme/': [
      //   {
      //     title: 'Tui主题',
      //     collapsable: false,
      //     children: [
      //       'base'
      //     ]
      //   }
      // ],
    },
  },
  chainWebpack(config) {
    config.resolve.alias.set("core-js/library/fn", "core-js/features");
  },
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: /node_modules/,
          use: [
            "cache-loader",
            {
              loader: "babel-loader",
              options: {
                babelrc: false,
                configFile: false,
                presets: [
                  "@babel/preset-env", // 可以识别es6语法
                  "@vue/babel-preset-jsx", // 解析jsx语法
                ],
              },
            },
            {
              loader: "ts-loader",
              options: {
                appendTsxSuffixTo: [/\.vue$/, /\.md$/],
              },
            },
          ],
        },
      ],
    },
  },
  plugins: [
    [
      "vuepress-plugin-typescript",
      {
        tsLoaderOptions: {
          // ts-loader 的所有配置项
        },
      },
    ],
    [
      "sakura",
      {
        num: 10, // 默认数量
        show: true,
        zIndex: 2,
        img: {
          replace: false, // false 默认图 true 换图 需要填写httpUrl地址
          httpUrl: "/assets/img/sakura.png", // 绝对路径
        },
      },
    ],
    [
      "ribbon-animation",
      {
        size: 90, // 默认数据
        opacity: 0.3, //  透明度
        zIndex: 1, //  层级
        opt: {
          // 色带HSL饱和度
          colorSaturation: "80%",
          // 色带HSL亮度量
          colorBrightness: "60%",
          // 带状颜色不透明度
          colorAlpha: 0.65,
          // 在HSL颜色空间中循环显示颜色的速度有多快
          colorCycleSpeed: 6,
          // 从哪一侧开始Y轴 (top|min, middle|center, bottom|max, random)
          verticalPosition: "center",
          // 到达屏幕另一侧的速度有多快
          horizontalSpeed: 200,
          // 在任何给定时间，屏幕上会保留多少条带
          ribbonCount: 2,
          // 添加笔划以及色带填充颜色
          strokeSize: 0,
          // 通过页面滚动上的因子垂直移动色带
          parallaxAmount: -0.5,
          // 随着时间的推移，为每个功能区添加动画效果
          animateSections: true,
        },
        ribbonShow: false, //  点击彩带  true显示  false为不显示
        ribbonAnimationShow: true, // 滑动彩带
      },
    ],
    ["go-top"],
    [
      "dynamic-title",
      {
        showIcon: "/favicon.ico",
        showText: "欢迎回来 O(∩_∩)O~",
        hideIcon: "/favicon.ico",
        hideText: "失联中。。。快回来~",
        recoverTime: 2000,
      },
    ],
    "@vuepress-reco/extract-code",
  ],
};
